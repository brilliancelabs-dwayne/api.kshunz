<?php
App::uses('AppController', 'Controller');
/**
 * CourseHoles Controller
 *
 * @property CourseHole $CourseHole
 */
class CourseHolesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CourseHole->recursive = 0;
		$this->set('courseHoles', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CourseHole->exists($id)) {
			throw new NotFoundException(__('Invalid course hole'));
		}
		$options = array('conditions' => array('CourseHole.' . $this->CourseHole->primaryKey => $id));
		$this->set('courseHole', $this->CourseHole->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CourseHole->create();
			if ($this->CourseHole->save($this->request->data)) {
				$this->Session->setFlash(__('The course hole has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The course hole could not be saved. Please, try again.'));
			}
		}
		$courses = $this->CourseHole->Course->find('list');
		$this->set(compact('courses'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CourseHole->exists($id)) {
			throw new NotFoundException(__('Invalid course hole'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->CourseHole->save($this->request->data)) {
				$this->Session->setFlash(__('The course hole has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The course hole could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CourseHole.' . $this->CourseHole->primaryKey => $id));
			$this->request->data = $this->CourseHole->find('first', $options);
		}
		$courses = $this->CourseHole->Course->find('list');
		$this->set(compact('courses'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CourseHole->id = $id;
		if (!$this->CourseHole->exists()) {
			throw new NotFoundException(__('Invalid course hole'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->CourseHole->delete()) {
			$this->Session->setFlash(__('Course hole deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Course hole was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
