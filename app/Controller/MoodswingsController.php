<?php
App::uses('AppController', 'Controller');
/**
 * Moodswings Controller
 *
 * @property Moodswing $Moodswing
 */
class MoodswingsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Moodswing->recursive = 0;
		$this->set('moodswings', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Moodswing->exists($id)) {
			throw new NotFoundException(__('Invalid moodswing'));
		}
		$options = array('conditions' => array('Moodswing.' . $this->Moodswing->primaryKey => $id));
		$this->set('moodswing', $this->Moodswing->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Moodswing->create();
			if ($this->Moodswing->save($this->request->data)) {
				$this->Session->setFlash(__('The moodswing has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The moodswing could not be saved. Please, try again.'));
			}
		}
		$rounds = $this->Moodswing->Round->find('list');
		$clubs = $this->Moodswing->Club->find('list');
		$this->set(compact('rounds', 'clubs'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Moodswing->exists($id)) {
			throw new NotFoundException(__('Invalid moodswing'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Moodswing->save($this->request->data)) {
				$this->Session->setFlash(__('The moodswing has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The moodswing could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Moodswing.' . $this->Moodswing->primaryKey => $id));
			$this->request->data = $this->Moodswing->find('first', $options);
		}
		$rounds = $this->Moodswing->Round->find('list');
		$clubs = $this->Moodswing->Club->find('list');
		$this->set(compact('rounds', 'clubs'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Moodswing->id = $id;
		if (!$this->Moodswing->exists()) {
			throw new NotFoundException(__('Invalid moodswing'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Moodswing->delete()) {
			$this->Session->setFlash(__('Moodswing deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Moodswing was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
