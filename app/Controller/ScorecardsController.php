<?php
App::uses('AppController', 'Controller');
/**
 * Scorecards Controller
 *
 * @property Scorecard $Scorecard
 */
class ScorecardsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Scorecard->recursive = 0;
		$this->set('scorecards', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Scorecard->exists($id)) {
			throw new NotFoundException(__('Invalid scorecard'));
		}
		$options = array('conditions' => array('Scorecard.' . $this->Scorecard->primaryKey => $id));
		$this->set('scorecard', $this->Scorecard->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Scorecard->create();
			if ($this->Scorecard->save($this->request->data)) {
				$this->Session->setFlash(__('The scorecard has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The scorecard could not be saved. Please, try again.'));
			}
		}
		$rounds = $this->Scorecard->Round->find('list');
		$moodswings = $this->Scorecard->Moodswing->find('list');
		$this->set(compact('rounds', 'moodswings'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Scorecard->exists($id)) {
			throw new NotFoundException(__('Invalid scorecard'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Scorecard->save($this->request->data)) {
				$this->Session->setFlash(__('The scorecard has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The scorecard could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Scorecard.' . $this->Scorecard->primaryKey => $id));
			$this->request->data = $this->Scorecard->find('first', $options);
		}
		$rounds = $this->Scorecard->Round->find('list');
		$moodswings = $this->Scorecard->Moodswing->find('list');
		$this->set(compact('rounds', 'moodswings'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Scorecard->id = $id;
		if (!$this->Scorecard->exists()) {
			throw new NotFoundException(__('Invalid scorecard'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Scorecard->delete()) {
			$this->Session->setFlash(__('Scorecard deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Scorecard was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
