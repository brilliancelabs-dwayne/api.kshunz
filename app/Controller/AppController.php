<?php

App::uses('Controller', 'Controller');

class AppController extends Controller {
	
	public $components = array(
			//'Auth',
			'RequestHandler'
	);
	
	public function beforeFilter(){
				
		//Since only API calls will be used serialize every controller name
		$name = inflector::underscore($this->name);
		
		$this->set('_serialize', $name);
				
	}
	
}
