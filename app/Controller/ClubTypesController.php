<?php
App::uses('AppController', 'Controller');
/**
 * ClubTypes Controller
 *
 * @property ClubType $ClubType
 */
class ClubTypesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ClubType->recursive = 0;
		$this->set('clubTypes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ClubType->exists($id)) {
			throw new NotFoundException(__('Invalid club type'));
		}
		$options = array('conditions' => array('ClubType.' . $this->ClubType->primaryKey => $id));
		$this->set('clubType', $this->ClubType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ClubType->create();
			if ($this->ClubType->save($this->request->data)) {
				$this->Session->setFlash(__('The club type has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The club type could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ClubType->exists($id)) {
			throw new NotFoundException(__('Invalid club type'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ClubType->save($this->request->data)) {
				$this->Session->setFlash(__('The club type has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The club type could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ClubType.' . $this->ClubType->primaryKey => $id));
			$this->request->data = $this->ClubType->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ClubType->id = $id;
		if (!$this->ClubType->exists()) {
			throw new NotFoundException(__('Invalid club type'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ClubType->delete()) {
			$this->Session->setFlash(__('Club type deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Club type was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
