<?php
/**
 * RoundFixture
 *
 */
class RoundFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'device_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'course_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'Active' => array('type' => 'string', 'null' => false, 'default' => '1', 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Created' => array('type' => 'date', 'null' => true, 'default' => null),
		'Updated' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'DynKey' => array('column' => array('device_id', 'course_id'), 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'device_id' => 1,
			'course_id' => 1,
			'Active' => 'Lorem ipsum dolor sit ame',
			'Created' => '2013-06-29',
			'Updated' => 1372545495
		),
	);

}
