<?php
/**
 * MoodswingFixture
 *
 */
class MoodswingFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'round_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'club_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'Active' => array('type' => 'string', 'null' => false, 'default' => '1', 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'HoleNumber' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4),
		'Stroke' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4),
		'Mood' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4),
		'Created' => array('type' => 'date', 'null' => true, 'default' => null),
		'Updated' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'DynKey' => array('column' => array('round_id', 'club_id'), 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'round_id' => 1,
			'club_id' => 1,
			'Active' => 'Lorem ipsum dolor sit ame',
			'HoleNumber' => 1,
			'Stroke' => 1,
			'Mood' => 1,
			'Created' => '2013-06-29',
			'Updated' => 1372545490
		),
	);

}
