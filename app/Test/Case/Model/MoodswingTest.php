<?php
App::uses('Moodswing', 'Model');

/**
 * Moodswing Test Case
 *
 */
class MoodswingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.moodswing',
		'app.round',
		'app.club',
		'app.device',
		'app.course',
		'app.course_hole',
		'app.user',
		'app.club_type',
		'app.scorecard'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Moodswing = ClassRegistry::init('Moodswing');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Moodswing);

		parent::tearDown();
	}

}
