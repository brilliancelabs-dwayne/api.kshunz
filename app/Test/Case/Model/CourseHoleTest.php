<?php
App::uses('CourseHole', 'Model');

/**
 * CourseHole Test Case
 *
 */
class CourseHoleTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.course_hole',
		'app.course'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CourseHole = ClassRegistry::init('CourseHole');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CourseHole);

		parent::tearDown();
	}

}
