<?php
App::uses('Scorecard', 'Model');

/**
 * Scorecard Test Case
 *
 */
class ScorecardTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.scorecard',
		'app.round',
		'app.device',
		'app.club',
		'app.club_type',
		'app.moodswing',
		'app.course',
		'app.course_hole',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Scorecard = ClassRegistry::init('Scorecard');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Scorecard);

		parent::tearDown();
	}

}
