<?php
App::uses('ClubType', 'Model');

/**
 * ClubType Test Case
 *
 */
class ClubTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.club_type',
		'app.club'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ClubType = ClassRegistry::init('ClubType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ClubType);

		parent::tearDown();
	}

}
