<?php
App::uses('ScorecardsController', 'Controller');

/**
 * ScorecardsController Test Case
 *
 */
class ScorecardsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.scorecard',
		'app.round',
		'app.device',
		'app.club',
		'app.club_type',
		'app.moodswing',
		'app.course',
		'app.course_hole',
		'app.user'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
