<?php
App::uses('MoodswingsController', 'Controller');

/**
 * MoodswingsController Test Case
 *
 */
class MoodswingsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.moodswing',
		'app.round',
		'app.club',
		'app.device',
		'app.course',
		'app.course_hole',
		'app.user',
		'app.club_type',
		'app.scorecard'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
